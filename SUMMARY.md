# Summary



* [1.Project manage](https://git-scm.com/)
    * [Assessment](doc/1projectmanage/Assessment1project-manage.md)
    * [Tool](doc/1projectmanage/Tool1Project-manage.md)
    * [How to use Git official document](https://git-scm.com/docs/gittutorial)
    * [Git](doc/1projectmanage/2git.md)
    * [Gitbook](doc/1projectmanage/4gitbook.md)
    * [GitFAQ](doc/1projectmanage/3gitFAQ.md)
    * [Markdown](doc/1projectmanage/markdown.md)
    * [Image uploader service](doc/1projectmanage/imageuploadservice.md)
    * [practice](/gitbook/1projectmange/practice.md) 
* [2.CAD design](https://www.autodesk.com/products/fusion-360/overview)
    * [practive](/gitbook/2cad/prqctive.md)        
* [3.3D printer](https://www.autodesk.com/products/fusion-360/overview)     
    * [practice](/gitbook/33Dprinter/practice.md)
* [4.Electric design](https://www.autodesk.com/products/fusion-360/overview) 
  